/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uam.eps.bmi.search.index.structure.impl;

import es.uam.eps.bmi.search.index.structure.Posting;
import es.uam.eps.bmi.search.index.structure.PostingsList;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import org.apache.lucene.index.PostingsEnum;

/**
 *
 * @author USUARIO
 */
public class PostingsListImpl implements PostingsList{
    List<Posting> postings;
    
    public PostingsListImpl(List<Posting> p) {
        postings = p;
    }

    public Iterator<Posting> iterator() {
        return postings.iterator();
    }

    public int size() {
        return postings.size();
    }
    
}
