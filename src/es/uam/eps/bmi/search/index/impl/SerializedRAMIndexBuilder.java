/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uam.eps.bmi.search.index.impl;

import es.uam.eps.bmi.search.index.AbstractIndexBuilder;
import static es.uam.eps.bmi.search.index.Config.*;
import es.uam.eps.bmi.search.index.Index;
import es.uam.eps.bmi.search.index.structure.Posting;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.TreeMap;

/**
 *
 * @author e282254
 */
public class SerializedRAMIndexBuilder extends AbstractIndexBuilder {

    private String path;
    private int actualID = 0;
    private TreeMap<String, List<Posting>> diccionario;
    private List<String> docPaths = new ArrayList<String>();
    private FileWriter w;
    private int totalpalabras = 0;

    @Override
    protected void indexText(String text, String path) throws IOException {
        boolean flag = false;
        
        this.docPaths.add(path);
        for (String p : text.split("\\P{Alpha}+")) {
            w.write(totalpalabras + "\t" + diccionario.size()+"\n");
            p=p.toLowerCase();
            if (diccionario.containsKey(p)) {
                List<Posting> post = diccionario.get(p);
                for (Posting po : post) {
                    if (po.getDocID() == actualID) {
                        flag = true;
                        po.add1();
                    }
                }
                if (!flag) {
                    Posting pos = new Posting(actualID, 1);
                    post.add(pos);
                }
                flag = false;
            } else {
                List<Posting> post = new ArrayList<Posting>();
                Posting po = new Posting(actualID, 1);
                post.add(po);
                diccionario.put(p, post);
            }
            totalpalabras++;

        }

        actualID++;
        
    }

    @Override
    protected Index getCoreIndex() throws IOException {
        return new SerializedRAMIndex(diccionario,actualID,path);
    }

    @Override
    public void build(String collectionPath, String indexPath) throws IOException {
        
        this.actualID = 0;
        this.path = indexPath;
        this.diccionario = new TreeMap<String, List<Posting>>();
        
        /*Comprobar si el directorio index existe*/
        clear(indexPath);

        FileWriter writerPaths = new FileWriter(indexPath + pathsFileName);
        w = new FileWriter(indexPath + "leydeheap.txt");
        
        File docDir = new File(collectionPath);
        if (collectionPath.endsWith(".zip")) {
            indexZip(docDir);
        } else if (collectionPath.endsWith(".txt")) {
            indexURLs(docDir);
        } else {
            indexFolder(docDir);
        }
        w.close();
        
        /*Paths de los documentos*/
        for(String s: this.docPaths){
            writerPaths.write(s + "\n");
        }
        writerPaths.close();
   
        /*Serial indice*/
        FileOutputStream f;
        f = new FileOutputStream(indexPath + indexFileName);

        ObjectOutputStream obj;
        obj = new ObjectOutputStream(f);
        obj.writeInt(actualID);
        obj.writeObject(diccionario);
        
        f.close();
        obj.close();
        //CLOSE DE TODO NO???
        
        /*Modulo documentos*/
        saveDocNorms(path);

    }


}
