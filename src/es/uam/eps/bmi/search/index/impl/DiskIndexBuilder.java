/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uam.eps.bmi.search.index.impl;

import es.uam.eps.bmi.search.index.AbstractIndexBuilder;
import static es.uam.eps.bmi.search.index.Config.*;
import es.uam.eps.bmi.search.index.Index;
import es.uam.eps.bmi.search.index.structure.Posting;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.ObjectOutputStream;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

/**
 *
 * @author e280752
 */
public class DiskIndexBuilder extends AbstractIndexBuilder {

    private String path;
    private int actualID = 0;
    private TreeMap<String, List<Posting>> diccionario;
    private List<String> docPaths = new ArrayList<String>();
    private TreeMap<String, Long> diccionario2;

    @Override
    protected void indexText(String text, String path) throws IOException {
        boolean flag = false;

        this.docPaths.add(path);

        for (String p : text.split("\\P{Alpha}+")) {
            p = p.toLowerCase();
            if (diccionario.containsKey(p)) {
                List<Posting> post = diccionario.get(p);
                for (Posting po : post) {
                    if (po.getDocID() == actualID) {
                        flag = true;
                        po.add1();
                    }
                }
                if (!flag) {
                    Posting pos = new Posting(actualID, 1);
                    post.add(pos);
                }
                flag = false;
            } else {
                List<Posting> post = new ArrayList<Posting>();
                Posting po = new Posting(actualID, 1);
                post.add(po);
                diccionario.put(p, post);
            }

        }

        actualID++;
    }

    @Override
    protected Index getCoreIndex() throws IOException {
        return new DiskIndex(diccionario2, actualID, path);
    }

    @Override
    public void build(String collectionPath, String indexPath) throws IOException {
        this.actualID = 0;
        this.path = indexPath;
        this.diccionario = new TreeMap<String, List<Posting>>();
        this.diccionario2 = new TreeMap<String, Long>();

        /*Comprobar si el directorio index existe*/
        clear(indexPath);

        FileWriter writerPaths = new FileWriter(indexPath + pathsFileName);

        File docDir = new File(collectionPath);
        if (collectionPath.endsWith(".zip")) {
            indexZip(docDir);
        } else if (collectionPath.endsWith(".txt")) {
            indexURLs(docDir);
        } else {
            indexFolder(docDir);
        }

        /*Paths de los documentos*/
        for (String s : this.docPaths) {
            writerPaths.write(s + "\n");
        }
        writerPaths.close();

        //Fichero de diccionario
        RandomAccessFile fdicc;
        fdicc = new RandomAccessFile(indexPath + dictionaryFileName, "rw");

        //Fichero de Postings
        RandomAccessFile fpost;
        fpost = new RandomAccessFile(indexPath + postingsFileName, "rw");

        long puntero = 0;
        for (Map.Entry<String, List<Posting>> k : diccionario.entrySet()) {
            puntero = fpost.getFilePointer();
            //Diccionario: (longitud del termino, termino, puntero a lista de postings)
            fdicc.writeInt(k.getKey().length());
            fdicc.writeChars(k.getKey());
            fdicc.writeLong(puntero);
            diccionario2.put(k.getKey(), puntero);

            fpost.writeInt(k.getValue().size());
            for (Posting po : k.getValue()) {
                fpost.writeInt(po.getDocID());
                fpost.writeLong(po.getFreq());
            }
        }
        fdicc.close();
        fpost.close();

        /*Modulo documentos*/
        saveDocNorms(path);
    }

}
