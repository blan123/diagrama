/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uam.eps.bmi.search.index.impl;

import es.uam.eps.bmi.search.index.AbstractIndex;
import static es.uam.eps.bmi.search.index.Config.*;
import es.uam.eps.bmi.search.index.structure.Posting;
import es.uam.eps.bmi.search.index.structure.PostingsList;
import es.uam.eps.bmi.search.index.structure.impl.PostingsListImpl;
import java.io.BufferedReader;
import java.io.EOFException;
import java.io.FileInputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.RandomAccessFile;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author e280752
 */
public class DiskIndex extends AbstractIndex {

    protected TreeMap<String, Long> dicc;
    private int num;
    private List<String> paths;
    private List<Double> norms;

    public DiskIndex(String path) throws IOException {
        super(path);
    }

    public DiskIndex(TreeMap<String, Long> diccionario, int numero, String path) throws IOException {
        indexFolder = path;
        num = numero;
        this.dicc = diccionario;
    }

    @Override
    public int numDocs() {
        return num;
    }

    @Override
    public PostingsList getPostings(String term) throws IOException {
        List<Posting> lista = new ArrayList<Posting>();

        RandomAccessFile f;
        f = new RandomAccessFile(indexFolder + postingsFileName, "r");

        //lo mismo para SERIALIZED
        long puntero = 0;
        if(dicc.get(term) !=null)
            puntero = dicc.get(term);
        else
            return null; //¿Como hacemos el control de errores?

        f.seek(puntero);
        int tamano = f.readInt();

        for (int i = 0; i < tamano; i++) {
            Posting p = new Posting(f.readInt(), f.readLong());
            lista.add(p);
        }

        f.close();

        return new PostingsListImpl(lista);
    }

    @Override
    public Collection<String> getAllTerms() throws IOException {
        List<String> termList = new ArrayList<String>();
        for (Map.Entry<String, Long> k : dicc.entrySet()) {
            termList.add(k.getKey());
        }
        return termList;
    }

    @Override
    public long getTotalFreq(String term) throws IOException {
        long totalFreq = 0;
        for (Posting po : this.getPostings(term)) {
            totalFreq = totalFreq + po.getFreq();
        }
        return totalFreq;
    }

    @Override
    public long getDocFreq(String term) throws IOException {
        return this.getPostings(term).size();
    }

    @Override
    public void load(String path) throws IOException {
        this.paths = new ArrayList<String>();
        this.norms = new ArrayList<Double>();
        this.dicc = new TreeMap<String, Long>();

        //Paths
        FileReader filePaths = new FileReader(path + pathsFileName);
        BufferedReader buff = new BufferedReader(filePaths);
        String linea;
        int contador = 0;
        while ((linea = buff.readLine()) != null) {
            this.paths.add(linea);
            contador++;
        }
        buff.close();
        filePaths.close();
        this.num = contador;

        //Normas
        FileReader fileNorms = new FileReader(path + normsFileName);
        BufferedReader buff2 = new BufferedReader(fileNorms);
        while ((linea = buff2.readLine()) != null) {
            norms.add(Double.parseDouble(linea));
        }
        buff2.close();
        fileNorms.close();

        //Diccionario
        RandomAccessFile fdicc;
        fdicc = new RandomAccessFile(path + dictionaryFileName, "r");
        String palabra = "";
        long punter = 0;
        int tam = 0;
        try {
            while (true) {
                tam = fdicc.readInt();
                for (int j = 0; j < tam; j++) {
                    palabra = palabra + fdicc.readChar();
                }
                punter = fdicc.readLong();
                dicc.put(palabra, punter);
                palabra = "";
            }
        } catch (EOFException e) {
            fdicc.close();
            
        }

    }

    @Override
    public String getDocPath(int docID) throws IOException {
        return paths.get(docID);
    }

    @Override
    public double getDocNorm(int docID) throws IOException {
        return norms.get(docID);
    }

}
