/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uam.eps.bmi.search.index.impl;

import es.uam.eps.bmi.search.index.AbstractIndex;
import es.uam.eps.bmi.search.index.Config;
import static es.uam.eps.bmi.search.index.Config.*;
import es.uam.eps.bmi.search.index.structure.Posting;
import es.uam.eps.bmi.search.index.structure.PostingsList;
import es.uam.eps.bmi.search.index.structure.impl.PostingsListImpl;
import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.Map.Entry;
import java.util.TreeMap;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.lucene.index.MultiFields;
import org.apache.lucene.index.TermsEnum;

/**
 *
 * @author e282254
 */
public class SerializedRAMIndex extends AbstractIndex {
    
    protected TreeMap<String, List<Posting>> dicc;
    private int num;
    private List<String> paths;
    private List<Double> norms;

    
    public SerializedRAMIndex(String path) throws IOException {
        super(path);
    }
    
    public SerializedRAMIndex(TreeMap<String, List<Posting>> diccionario, int numero, String path) throws IOException {
        indexFolder = path;
        num=numero;
        dicc=diccionario;
    }
    
    @Override
    public int numDocs() {
        
        return num;
        
    }

    @Override
    public PostingsList getPostings(String term) throws IOException {
        return new PostingsListImpl(dicc.get(term));
    }

    @Override
    public Collection<String> getAllTerms() throws IOException {
        List<String> termList = new ArrayList<String>();
        for (Entry<String, List<Posting>> k : dicc.entrySet()) {
            termList.add(k.getKey());
        }
        return termList;
    }

    //suma de frecuencia del termino en todos los documentos
    @Override
    public long getTotalFreq(String term) throws IOException {
        long totalFreq=0;
        for (Posting po: dicc.get(term)){
            totalFreq = totalFreq + po.getFreq();
        }
        return totalFreq;
    }

    //numero de documentos que contienen el termino
    @Override
    public long getDocFreq(String term) throws IOException {
        return dicc.get(term).size();
    }

    @Override
    public void load(String path) throws IOException {
        this.paths = new ArrayList<String>();
        this.norms = new ArrayList<Double>();
        this.dicc = new TreeMap<String, List<Posting>>();
        
        //Diccionario completo
        FileInputStream f;
        f = new FileInputStream(path + indexFileName);
        ObjectInputStream obj;
        obj = new ObjectInputStream(f);
        
        //Paths
        FileReader filePaths = new FileReader(path + pathsFileName);
        BufferedReader buff = new BufferedReader(filePaths);
        String linea;
        while ((linea=buff.readLine()) != null){
            this.paths.add(linea);
        }
        buff.close();
        filePaths.close();
        
        //Normas
        FileReader fileNorms = new FileReader(path + normsFileName);
        BufferedReader buff2 = new BufferedReader(fileNorms);
        while ((linea=buff2.readLine()) != null){
            norms.add(Double.parseDouble(linea));
        }
        buff2.close();
        fileNorms.close();
        
        try {
            num=obj.readInt();
            dicc=(TreeMap<String, List<Posting>>)obj.readObject();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(SerializedRAMIndex.class.getName()).log(Level.SEVERE, null, ex);
        }
        obj.close();
        f.close();
        
    }

    @Override
    public String getDocPath(int docID) throws IOException {
        return paths.get(docID);
    }

    @Override
    public double getDocNorm(int docID) throws IOException {
        return norms.get(docID);
    }
    
}
