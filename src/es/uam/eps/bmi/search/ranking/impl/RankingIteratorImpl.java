/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uam.eps.bmi.search.ranking.impl;

import es.uam.eps.bmi.search.ranking.SearchRankingDoc;
import es.uam.eps.bmi.search.ranking.SearchRankingIterator;
import es.uam.eps.bmi.search.index.Index;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Iterator;
import java.util.PriorityQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

public class RankingIteratorImpl implements SearchRankingIterator {

    private PriorityQueue<RankingDocImpl> results;   
    Index index;
    private Iterator<RankingDocImpl> iterator;

    public RankingIteratorImpl(Index idx, PriorityQueue<RankingDocImpl> r) {
        ArrayList<RankingDocImpl> list = new ArrayList<RankingDocImpl>(r);
        Collections.sort(list);
        index = idx;
        results = r;
        iterator = list.iterator();

    }

    public RankingIteratorImpl() {
        index = null;
        results = new PriorityQueue<RankingDocImpl>();
    }

    @Override
    public boolean hasNext() {
        return iterator.hasNext();
    }

    @Override
    public SearchRankingDoc next() {
        
        return iterator.next();
    }

}
