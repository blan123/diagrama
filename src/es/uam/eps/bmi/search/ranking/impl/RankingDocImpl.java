/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uam.eps.bmi.search.ranking.impl;

import es.uam.eps.bmi.search.index.Index;
import es.uam.eps.bmi.search.ranking.SearchRankingDoc;
import java.io.IOException;

public class RankingDocImpl extends SearchRankingDoc {
    
    Index index;
    private double score;
    private int docId;
    
    public RankingDocImpl (Index idx, double s, int p) {
        index = idx;
        score = s;
        docId = p;
    }

    @Override
    public double getScore(){
        return this.score;
    }

    @Override
    public String getPath() throws IOException {
        return this.index.getDocPath(docId);
    }
    
    public int getDocID(){
        return this.docId;        
    }
    
    public void setScore(double sc){
        this.score=sc;
    }
            
    
}
