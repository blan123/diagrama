/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uam.eps.bmi.search.ranking.impl;

import es.uam.eps.bmi.search.index.Index;
import es.uam.eps.bmi.search.ranking.SearchRanking;
import es.uam.eps.bmi.search.ranking.SearchRankingDoc;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.Iterator;
import java.util.PriorityQueue;

public class RankingImpl implements SearchRanking {

    private PriorityQueue<RankingDocImpl> docs;
    private Index index;
    private int cut;

    public RankingImpl(Index idx, int cutoff) {
        index = idx;
        cut = cutoff;
        docs = new PriorityQueue<RankingDocImpl>(Comparator.reverseOrder());
    }

    @Override
    public int size() {
        return docs.size();
    }

    @Override
    public Iterator<SearchRankingDoc> iterator() {

        return new RankingIteratorImpl(this.index, docs);
    }

    public void add(int doc, double score) throws IOException {

        if (docs.size() >= cut) {
            if (docs.peek().getScore() < score) {
                RankingDocImpl rd = new RankingDocImpl(index, score, doc);
                docs.add(rd);
                docs.poll();
            }

        } else {
            RankingDocImpl rd = new RankingDocImpl(index, score, doc);
            docs.add(rd);
        }

    }

}
