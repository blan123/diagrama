/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uam.eps.bmi.search.vsm;

import es.uam.eps.bmi.search.AbstractEngine;
import es.uam.eps.bmi.search.index.Index;
import es.uam.eps.bmi.search.index.structure.Posting;
import es.uam.eps.bmi.search.index.structure.PostingsList;
import es.uam.eps.bmi.search.ranking.SearchRanking;
import es.uam.eps.bmi.search.ranking.impl.RankingImpl;
import static es.uam.eps.bmi.search.vsm.AbstractVSMEngine.tfidf;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 *
 * @author USUARIO
 */
public class DocBasedVSMEngine extends AbstractVSMEngine {

    public DocBasedVSMEngine(Index index) {
        super(index);
    }

    @Override
    public SearchRanking search(String query, int cutoff) throws IOException {
        RankingImpl ranking = new RankingImpl(index, cutoff);
        String q[] = query.split(" ");
        ArrayList<Iterator<Posting>> itlist = new ArrayList<Iterator<Posting>>();
        PriorityQueue<SpecialPosting> heap = new PriorityQueue<SpecialPosting>();
        HashMap<Integer, Double> mapa = new HashMap<Integer, Double>();
        HashMap<Integer, Integer> sizePostingsLists = new HashMap<Integer, Integer>();

        int a = 0;
        for (String palabra : q) {
            PostingsList posts = index.getPostings(palabra);
            itlist.add(posts.iterator());
            sizePostingsLists.put(a, posts.size());
            a++;
        }
        int c = 0;
        for (Iterator<Posting> it : itlist) {
            if (it.hasNext()) {
                SpecialPosting sp = new SpecialPosting(it.next(), c);
                c++;
                heap.add(sp);
            }
        }
        double score = 0;
        SpecialPosting spos = heap.poll();
        Posting pos = spos.getPosting();
        int idl = spos.getIdLista();
        int actualID = pos.getDocID();
        while (!heap.isEmpty()) {
            if (actualID == pos.getDocID()) {
                score = score + (tfidf(pos.getFreq(), sizePostingsLists.get(idl), index.numDocs())) / index.getDocNorm(pos.getDocID());
            } else {
                mapa.put(actualID, score);
                actualID = pos.getDocID();
                score = (tfidf(pos.getFreq(), sizePostingsLists.get(idl), index.numDocs())) / index.getDocNorm(pos.getDocID());
            }
            if(itlist.get(idl).hasNext())
                heap.add(new SpecialPosting(itlist.get(idl).next(), idl));
            
            spos = heap.poll();
            pos = spos.getPosting();
            idl = spos.getIdLista();
            
            if(heap.isEmpty()){
                if(itlist.get(idl).hasNext())
                    heap.add(new SpecialPosting(itlist.get(idl).next(), idl));
            }
        }
        
        mapa.put(actualID, score);
        score = (tfidf(pos.getFreq(), sizePostingsLists.get(idl), index.numDocs())) / index.getDocNorm(pos.getDocID());
        mapa.put(pos.getDocID(),score);
        
        SortedSet<Map.Entry<Integer, Double>> orden = entriesSortedByValues(mapa);
        Iterator it = orden.iterator();
        
        Map.Entry e1;
        
        while(it.hasNext()){
            e1 = (Map.Entry) it.next();
            ranking.add((Integer) e1.getKey(), (Double)e1.getValue());
        }
        return ranking;
        
        
    }
    
    static <K, V extends Comparable<? super V>> SortedSet<Map.Entry<K, V>> entriesSortedByValues(Map<K, V> map) {
        SortedSet<Map.Entry<K, V>> sortedEntries = new TreeSet<Map.Entry<K, V>>(new Comparator<Map.Entry<K, V>>() {
            @Override
            public int compare(Map.Entry<K, V> e1, Map.Entry<K, V> e2) {
                int res = e2.getValue().compareTo(e1.getValue());
                return res != 0 ? res : 1;
            }
        }
        );
        sortedEntries.addAll(map.entrySet());
        return sortedEntries;
    }

    private static class SpecialPosting implements Comparable<SpecialPosting>{

        private Posting posting;
        private int idLista;

        public SpecialPosting(Posting p, int i) {
            posting = p;
            idLista = i;
        }

        public Posting getPosting() {
            return posting;
        }

        public int getIdLista() {
            return idLista;
        }

        public int compareTo(SpecialPosting o) {
            return posting.getDocID() - o.getPosting().getDocID();
        }
    }

}
