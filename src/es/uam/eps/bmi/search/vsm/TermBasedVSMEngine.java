/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package es.uam.eps.bmi.search.vsm;

import es.uam.eps.bmi.search.AbstractEngine;
import es.uam.eps.bmi.search.index.Index;
import es.uam.eps.bmi.search.index.structure.Posting;
import es.uam.eps.bmi.search.index.structure.PostingsList;
import es.uam.eps.bmi.search.ranking.SearchRanking;
import es.uam.eps.bmi.search.ranking.impl.RankingDocImpl;
import es.uam.eps.bmi.search.ranking.impl.RankingImpl;
import java.io.IOException;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;

/**
 *
 * @author biblioteca
 */
public class TermBasedVSMEngine extends AbstractVSMEngine {

    public TermBasedVSMEngine(Index index) {
        super(index);
    }

    @Override
    public SearchRanking search(String query, int cutoff) throws IOException {

        double total = 0, tfidf = 0;

        HashMap<Integer, Double> mapa = new HashMap<Integer, Double>();
        RankingImpl ranking = new RankingImpl(index, cutoff);
        String q[] = query.split(" ");
        for (String palabra : q) {
            PostingsList posts = index.getPostings(palabra);
            for (Posting p : posts) {
                tfidf = (tfidf(p.getFreq(), posts.size(), index.numDocs())) / index.getDocNorm(p.getDocID());
                if (!mapa.containsKey(p.getDocID())) {
                    mapa.put(p.getDocID(), tfidf);
                } else {
                    total = mapa.get(p.getDocID()) + tfidf;
                    mapa.put(p.getDocID(), total);

                }
            }

        }
        
        SortedSet<Map.Entry<Integer, Double>> orden = entriesSortedByValues(mapa);
        Iterator it = orden.iterator();
        
        Map.Entry e1;
        
        while(it.hasNext()){
            e1 = (Map.Entry) it.next();
            ranking.add((Integer) e1.getKey(), (Double)e1.getValue());
        }
        return ranking;
    }

    static <K, V extends Comparable<? super V>> SortedSet<Map.Entry<K, V>> entriesSortedByValues(Map<K, V> map) {
        SortedSet<Map.Entry<K, V>> sortedEntries = new TreeSet<Map.Entry<K, V>>(new Comparator<Map.Entry<K, V>>() {
            @Override
            public int compare(Map.Entry<K, V> e1, Map.Entry<K, V> e2) {
                int res = e2.getValue().compareTo(e1.getValue());
                return res != 0 ? res : 1;
            }
        }
        );
        sortedEntries.addAll(map.entrySet());
        return sortedEntries;
    }

    //hola
    //hacer hashmap con docIDs y scores
    //recorrer palabras de la query  y con cada una los documentos (se suma idf*tf cuando ya este en el hashmap)
}
